package com.unialfa.ia;

import com.unialfa.ia.daoInstance.GetInstance;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Debug;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

public class Main {

    public static void main(String[] args) {
	// write your code here

        //------------------------------------------------------
        // (1) importação da base de dados de treinamento
        //------------------------------------------------------
        String DATASETPATH = "C:\\Users\\lucas.pereira\\IdeaProjects\\MLP-IA\\src\\com\\unialfa\\ia\\warehouse\\Autism-Child-Data.arff";
        String MODElPATH = "C:\\Users\\lucas.pereira\\IdeaProjects\\MLP-IA\\src\\com\\unialfa\\ia\\warehouse\\New-Autism-Child-Data.arff";

        //------------------------------------------------------
        // (2) Objetos de manipulação
        //------------------------------------------------------
        GetInstance instance = new GetInstance();
        ModelClassifier ModelClassifier = new ModelClassifier();
        buildClassifier buildClass = new buildClassifier();
        evaluateModel evaluateModel = new evaluateModel();

        //------------------------------------------------------
        // (3) Inicializando as instancias...
        //------------------------------------------------------
        Instances dataset = instance.loadDataset(DATASETPATH);

        //------------------------------------------------------
        // (4) Filtro de Normalização
        //------------------------------------------------------
        Filter filter = new Normalize();

        //------------------------------------------------------
        // (5) Divisão das instancias... 80-20
        //------------------------------------------------------
        int trainSize = (int) Math.round(dataset.numInstances() * 0.8);
        int testSize = dataset.numInstances() - trainSize;

        dataset.randomize(new Debug.Random(1));

        try {
            //------------------------------------------------------
            // (6) Normalize os dados
            //------------------------------------------------------
            filter.setInputFormat(dataset);
            Instances datasetnor = filter.useFilter(dataset, filter);

            //------------------------------------------------------
            // (7) Inicialização de Instancias de Treino e Teste
            //------------------------------------------------------

            Instances traindataset = new Instances(datasetnor, 0, trainSize);
            Instances testdataset = new Instances(datasetnor, trainSize, testSize);

            //------------------------------------------------------
            // (8) Criando o objeto MLP com um contrutor de MPL com cast de Build Classifier
            //------------------------------------------------------
            MultilayerPerceptron ann = (MultilayerPerceptron) buildClass.buildClassifier(traindataset);

            //------------------------------------------------------
            // (9) Imprimindo a evolução do modelo
            //------------------------------------------------------
            String evalSumary = evaluateModel.evaluateModel(ann, traindataset, testdataset);
            System.out.println("Evolucao: " + evalSumary);

            //------------------------------------------------------
            // (10) Salvando o modelo...
            //------------------------------------------------------
            evaluateModel.saveModel(ann, MODElPATH);


            //------------------------------------------------------
            // (11) Imprimir a classificão de uma instancia
            //------------------------------------------------------
            String className = ModelClassifier.classifiy(Filter.useFilter(ModelClassifier.createInstance(1.6, 0.2, 0), filter), MODElPATH);

            System.out.println("\n O nome da Classe para a Instancia com ASDlenght = 1.6 e ASDWidth = 0.2 é " + className);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



}
