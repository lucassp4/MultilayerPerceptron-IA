package com.unialfa.ia;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.SerializationHelper;

public class ModelClassifier {

    private Attribute  ASDlegth;
    private Attribute ASDwidth;

    private ArrayList attributes;
    private ArrayList classVal;
    private Instances dataRaw;

    public ModelClassifier(){

        ASDlegth = new Attribute("ASDlegth");
        ASDwidth = new Attribute("ASDwidth");

        attributes = new ArrayList();
        classVal = new ArrayList();
        classVal.add("YES");
        classVal.add("NO");

        attributes.add(ASDlegth);
        attributes.add(ASDwidth);


        attributes.add(new Attribute("Class/ASD", classVal));
        dataRaw = new Instances("TestInstance", attributes, 0);
        dataRaw.setClassIndex(dataRaw.numAttributes()-1);

    }

    public Instances createInstance(double ASDlegth, double ASDwidth, double result){
        dataRaw.clear();
        double[] instanceValue1 = new double[]{ASDlegth, ASDwidth,0};
        dataRaw.add(new DenseInstance(1.0,instanceValue1));
        return dataRaw;
    }


    public String classifiy(Instances insts, String path){
        String result = "Nâo Classificado!!";
        Classifier cls = null;
        try{
            cls = (MultilayerPerceptron) SerializationHelper.read(path);
            result = (String) classVal.get((int) cls.classifyInstance(insts.firstInstance()));
        }catch (Exception ex) {
            Logger.getLogger(ModelClassifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public Instances getInstance() {
        return dataRaw;
    }
}
