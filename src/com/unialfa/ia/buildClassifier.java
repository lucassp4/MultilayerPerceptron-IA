package com.unialfa.ia;

import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instances;


import java.util.logging.Level;
import java.util.logging.Logger;

public class buildClassifier {

    public Classifier buildClassifier(Instances train){

        MultilayerPerceptron mlp = new MultilayerPerceptron();

        /*Multipreceptron parameters and its default values
                *Learning Rate for the backpropagation algorithm (Value should be between 0 - 1, Default = 0.3).
                *m.setLearningRate(0);

	            *Momentum Rate for the backpropagation algorithm (Value should be between 0 - 1, Default = 0.2).
                *m.setMomentum(0);

                *Number of epochs to train through (Default = 500).
                *m.setTrainingTime(0)

                *Percentage size of validation set to use to terminate training (if this is non zero it can pre-empt num of epochs.
                (Value should be between 0 - 100, Default = 0).
                *m.setValidationSetSize(0);

	            *The value used to seed the random number generator (Value should be >= 0 and and a long, Default = 0).
                *m.setSeed(0);

                *The hidden layers to be created for the network(Value should be a list of comma separated Natural
                numbers or the letters 'a' = (attribs + classes) / 2,
                'i' = attribs, 'o' = classes, 't' = attribs .+ classes) for wildcard values, Default = a).
                *m.setHiddenLayers("2,3,3"); three hidden layer with 2 nodes in first layer and 3 nodends in second and 3 nodes in the third.

                *The desired batch size for batch prediction  (default 100).
                *m.setBatchSize("1");
         */


        try{
            //Setting Parameters
            mlp.setLearningRate(0.1);
            mlp.setMomentum(0.2);
            mlp.setTrainingTime(2000);
            mlp.setHiddenLayers("3");
            mlp.buildClassifier(train);

            //Build
            mlp.buildClassifier(train);

        }catch (Exception e){

            Logger.getLogger(buildClassifier.class.getName()).log(Level.SEVERE, null, e);
        }
        return mlp;
    }
}
